import random
from icecream import ic
from main import set_ns_walksign, set_ew_walksign


class World:
    def __init__(self):
        # Stores all the active cars and pedestrians
        self.cars = []
        self.pedestrians = []

        # Current info about the state
        self.state = ALL_RED
        self.state_timer = 1
        self.yellow_lights = False
        self.ewwalk = False
        self.nswalk = False

    def set_ns_walksign(self, state):
        self.nswalk = state

    def set_ew_walksign(self, state):
        self.ewwalk = state


    def select_new_state(self):
        if (not self.cars and not self.pedestrians):
            # no cars or pedestrians, stay all red
            self.state = ALL_RED
            self.state_timer = 1
            ic(self.cars, self.pedestrians)
        elif (not self.cars):
            # pedestrians, no cars
            self.state = random.choice(self.pedestrians[0].wanted_conditions())
            self.state_timer = 11
        elif (not self.pedestrians):
            # cars, no pedestrians
            self.state = random.choice(self.cars[0].wanted_conditions())
            self.state_timer = 11
        elif bool(random.getrandbits(1)):
            # both cars and pedestrians, True => choose from pedestrians
            self.state = random.choice(self.pedestrians[0].wanted_conditions())
            self.state_timer = 11
        else:
            # both cars and pedestrians, False => choose from cars
            self.state = random.choice(self.cars[0].wanted_conditions())
            self.state_timer = 11

    # only leave the cars and peds that don't want the current state
    def let_cars_and_peds_through(self):
        self.cars = [car for car in self.cars if not (self.state in car.wanted_conditions())]
        self.pedestrians = [ped for ped in self.pedestrians if not (self.state in ped.wanted_conditions())]

    def tick(self):
        ic(self.state_timer)
        # if we've been letting traffic through and there's less than 5 seconds left, turn yellow
        if self.state != ALL_RED and self.state_timer <= 5:
            self.yellow_lights = True
        else:
            # if the lights are green, let cars and peds through
            self.yellow_lights = False
            self.let_cars_and_peds_through()


        # if we've run out of time, start a 5 second countdown for the ALL_RED state
        if self.state != ALL_RED and self.state_timer == 0:
            # then, set the state to all red
            ic(self.state)
            ic(self.state_timer)
            self.state = ALL_RED
            self.state_timer = 6
        elif self.state == ALL_RED and self.state_timer == 0:
            self.select_new_state()
        
        if self.state == EW_ONLY:
            self.set_ew_walksign(True)
            self.set_ns_walksign(False)
        elif self.state == NS_ONLY:
            self.set_ew_walksign(False)
            self.set_ns_walksign(True)
        else:
            print(self.state)
            self.set_ew_walksign(False)
            self.set_ns_walksign(False)
        self.state_timer -= 1



class Car:
    def __init__(self, location, goal):
        self.location = location
        self.goal = goal

    def wanted_conditions(self):
        if self.location in [NORTH, SOUTH] and self.goal == STRAIGHT:
            return [NS_ONLY, NS_THRU_RIGHT, NS_ALL]
        elif self.location in [EAST, WEST] and self.goal == STRAIGHT:
            return [EW_ONLY, EW_THRU_RIGHT, EW_ALL]
        elif self.location in [NORTH, SOUTH] and self.goal == LEFT:
            return [NS_LEFT_TURNS, NS_ALL]
        elif self.location in [EAST, WEST] and self.goal == LEFT:
            return [EW_LEFT_TURNS, EW_ALL]
        elif self.location in [NORTH, SOUTH] and self.goal == RIGHT:
            return [NS_THRU_RIGHT, NS_ALL]
        elif self.location in [EAST, WEST] and self.goal == RIGHT:
            return [EW_THRU_RIGHT, EW_ALL]

class Pedestrian:
    def __init__(self, location, goal):
        self.location = location
        self.goal = goal

    def wanted_conditions(self):
        if (self.location in [NW, NE] and self.goal in [NW, NE]) or (self.location in [SW, SE] and self.goal in [SW, SE]):
            return [EW_ONLY, PEDS_NO_CARS]
        if (self.location in [NW, SW] and self.goal in [NW, SW]) or (self.location in [NE, SE] and self.goal in [NE, SE]):
           return [NS_ONLY, PEDS_NO_CARS]
        


# Secure states
ALL_RED = "all_red"  # red lights+no walk sign in all directions
NS_ONLY = "ns_only"  # ns through traffic allowed (including peds), ew traffic not allowed (including pedestrian traffic)
EW_ONLY = "ew_only"  # ew through traffic allowed (including peds), ns traffic not allowed (including pedestrian traffic)
NS_LEFT_TURNS = "ns_left_turns"  # ns traffic allowed to turn left, all other lights red
EW_LEFT_TURNS = "ew_left_turns"  # ew traffic allowed to turn left, all other lights red
NS_THRU_RIGHT = "ns_thru_right"  # ns through traffic allowed, right turns allowed, no pedestrians allowed
EW_THRU_RIGHT = "ew_thru_right"  # ew trough traffic allowed, right turns allowed, no pedestrians allowed
NS_ALL = "ns_all"  # ns all car options allowed, none other allowed
EW_ALL = "ew_all"  # ew all car options allowed, none other allowed
PEDS_NO_CARS = "pwds_no_cars"  # All pedestrians allowed, no cars allowed
# ALL OTHER STATES ARE UNSECURE

# Constants
NORTH = "north"
SOUTH = "south"
EAST = "east"
WEST = "west"

LEFT = "left"
RIGHT = "right"
STRAIGHT = "straight"

SW = "southwest"
SE = "southeast"
NW = "northwest"
NE = "northeast"
