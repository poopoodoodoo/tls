#!/usr/bin/env python
import pygame
from math import ceil, floor
from classes import *


GRASSCOLOR = "forestgreen"
ROADCOLOR = "#0E0C1D"
WALKCOLOR = "#EBEBEB"
WALKOFF = "#E53C3C"
SPACERCOLOR = "#FFE607"
CROSSBG = "#515151"
WALKWID = 55
WALKBARSPACE = 25
LANEWID = 110
LANESPACE = 8
WALKTHICK = 12
SDIM = 800
DASHLEN = 15
DASHSPACE = 25

nswalk = False
ewwalk = False

# set_ns_walksign(True) -> pedestrians can walk in the N/S direction
# set_ns_walksign(False) -> pedestrians CAN NOT walk in the N/S direction
def set_ns_walksign(state):
    global nswalk
    nswalk = state

def set_ew_walksign(state):
    global ewwalk
    ewwalk = state

def cwh(x, y, w, h):
    return pygame.Rect(x - w/2, y - h/2, w, h)

def chw(y, x, h, w):
    return pygame.Rect(x - w/2, y - h/2, w, h)

def main():
    world = World()
    pygame.init()
    screen = pygame.display.set_mode((SDIM, SDIM))
    pygame.display.set_caption("TrafficLight Sim")
    clock = pygame.time.Clock()

    total_millis = 0

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return

        screen.fill(GRASSCOLOR)
        TWOLANEWID = LANEWID*2 + LANESPACE
        pygame.draw.rect(screen, ROADCOLOR, cwh(SDIM/2, SDIM/2, TWOLANEWID, SDIM))
        pygame.draw.rect(screen, ROADCOLOR, cwh(SDIM/2, SDIM/2, SDIM, TWOLANEWID))

        NDASH = ceil(SDIM / (DASHLEN + DASHSPACE))
        for i in range(NDASH + 2):
            pygame.draw.rect(screen, SPACERCOLOR, cwh(i * (DASHLEN + DASHSPACE), SDIM/2, DASHLEN, LANESPACE))
            pygame.draw.rect(screen, SPACERCOLOR, cwh(SDIM/2, i * (DASHLEN + DASHSPACE), LANESPACE, DASHLEN))
        BIGRECTDIM = TWOLANEWID + WALKWID*2 + LANESPACE*2
        pygame.draw.rect(screen, ROADCOLOR, cwh(SDIM/2, SDIM/2, TWOLANEWID, TWOLANEWID))

        ewclr = WALKCOLOR
        nsclr = WALKCOLOR
        if not world.nswalk:
            nsclr = WALKOFF
        if not world.ewwalk:
            ewclr = WALKOFF

        pygame.draw.rect(screen, nsclr, cwh(SDIM/2 - TWOLANEWID/2 - LANESPACE/2, SDIM/2, LANESPACE, TWOLANEWID))
        pygame.draw.rect(screen, nsclr, cwh(SDIM/2 - TWOLANEWID/2 - LANESPACE/2 - WALKWID, SDIM/2, LANESPACE, TWOLANEWID))
        pygame.draw.rect(screen, ewclr, chw(SDIM/2 - TWOLANEWID/2 - LANESPACE/2, SDIM/2, LANESPACE, TWOLANEWID))
        pygame.draw.rect(screen, ewclr, chw(SDIM/2 - TWOLANEWID/2 - LANESPACE/2 - WALKWID, SDIM/2, LANESPACE, TWOLANEWID))
        pygame.draw.rect(screen, nsclr, cwh(SDIM/2 + TWOLANEWID/2 + LANESPACE/2, SDIM/2, LANESPACE, TWOLANEWID))
        pygame.draw.rect(screen, nsclr, cwh(SDIM/2 + TWOLANEWID/2 + LANESPACE/2 + WALKWID, SDIM/2, LANESPACE, TWOLANEWID))
        pygame.draw.rect(screen, ewclr, chw(SDIM/2 + TWOLANEWID/2 + LANESPACE/2, SDIM/2, LANESPACE, TWOLANEWID))
        pygame.draw.rect(screen, ewclr, chw(SDIM/2 + TWOLANEWID/2 + LANESPACE/2 + WALKWID, SDIM/2, LANESPACE, TWOLANEWID))

        NWALKBARS = floor(TWOLANEWID / (WALKTHICK + WALKBARSPACE))
        for i in range(1, NWALKBARS):
            pygame.draw.rect(screen, nsclr, cwh(SDIM/2 - TWOLANEWID/2 - LANESPACE - WALKWID/2, SDIM/2 - TWOLANEWID/2 + WALKTHICK/2 + i * (WALKTHICK+WALKBARSPACE), WALKWID, WALKTHICK))
            pygame.draw.rect(screen, nsclr, cwh(SDIM/2 + TWOLANEWID/2 + LANESPACE + WALKWID/2, SDIM/2 - TWOLANEWID/2 + WALKTHICK/2 + i * (WALKTHICK+WALKBARSPACE), WALKWID, WALKTHICK))
            pygame.draw.rect(screen, ewclr, chw(SDIM/2 - TWOLANEWID/2 - LANESPACE - WALKWID/2, SDIM/2 - TWOLANEWID/2 + WALKTHICK/2 + i * (WALKTHICK+WALKBARSPACE), WALKWID, WALKTHICK))
            pygame.draw.rect(screen, ewclr, chw(SDIM/2 + TWOLANEWID/2 + LANESPACE + WALKWID/2, SDIM/2 - TWOLANEWID/2 + WALKTHICK/2 + i * (WALKTHICK+WALKBARSPACE), WALKWID, WALKTHICK))


        pygame.display.flip()
        clock.tick(60)
        total_millis += clock.get_time()
        # ic(total_millis)
        if total_millis >= 1000:
            world.tick()
            total_millis = 0  


if __name__ == "__main__":
    main()
    pygame.quit()
